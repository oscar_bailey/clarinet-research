COV_REPORT=term
COV_MODULE=clarinet.modem
TEST_FILES=tests/test_all.py

LINT_FILES=clarinet/modem.py
LINT_ARGS=--disable=C

all: test

debug-tests:
	python -m ipdb tests/test_all.py

test:
	python -m pytest --cov-report $(COV_REPORT) --cov=$(COV_MODULE) $(TEST_FILES)

test-realtime: TEST_FILES=tests/test_realtime.py
test-realtime: test

test-eval: TEST_FILES=tests/test_evaluation.py
test-eval: test

test-encoding: TEST_FILES=tests/test_encoding.py
test-encoding: test

test-verbose: TEST_FILES=tests/test_all.py
test-verbose: test

test-modulators: TEST_FILES=tests/test_modulation.py
test-modulators: test

test-html: COV_REPORT=html
test-html: test

lint:
	pylint $(LINT_ARGS) $(LINT_FILES)

lint-tests: LINT_FILES=tests/test_*.py
lint-tests: lint

lint-utilities: LINT_FILES=clarinet/read_wav.py clarinet/transmit.py clarinet/listen.py
lint-utilities: lint

lint-evaluation: LINT_FILES=clarinet/evaluation.py
lint-evaluation: lint

lint-syntax: LINT_FILES=clarinet/*.py
lint-syntax: LINT_ARGS=
lint-syntax: lint

profile-readwav:
	python profiling/profile_readwav.py

profile-realtime-receiver:
	python profiling/profile_realtime_receiver.py

type-check:
	mypy --show-traceback --ignore-missing-imports --check-untyped-defs clarinet/modem.py
