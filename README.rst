=====================================
ClariNet: Audible Wireless Networking
=====================================

Introduction
============

ClariNet provides the user with tools to develop and test the performance of
audible networking configurations.
