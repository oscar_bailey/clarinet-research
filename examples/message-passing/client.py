import argparse
import clarinet
clarinet.SAMPLE_RATE = 44100

DESCRIPTION = \
    'Chat room example'

class Packet:
    header_size = 2
    magic_num = 7
    #magic_num = 170
    def __init__(self, byte_string):
        self._byte_string = byte_string
        self._bytes = None

    @property
    def bytes(self):
        if self._bytes:
            return self._bytes
        string_size = len(self._byte_string)
        self._bytes = bytes([self.magic_num, string_size]) + self._byte_string
        return self._bytes


class NetworkManager:
    def __init__(self, client_id, callback):
        self.tx_modulator = clarinet.FSK(2, 50)
        self.rx_modulator = clarinet.FSK(2, 50)
        # Change frequency of modulators for different clients
        if client_id == 1:
            self.tx_modulator.base_freq = 4000
        else:
            self.rx_modulator.base_freq = 4000
        self.tx_network = clarinet.NetworkDescription(self.tx_modulator)
        self.rx_network = clarinet.NetworkDescription(self.rx_modulator)
        self.transmitter = clarinet.Transmitter(self.tx_network)
        self.receiver = clarinet.Receiver(self.rx_network)
        self._callback = callback
        self._packet_size = 0
        self._byte_buffer = b''
        self._packet_timeout = -1

    def _handle_message(self, byte_string):
        print(byte_string)
        if self._packet_size > len(self._byte_buffer):
            self._byte_buffer = self._byte_buffer + byte_string
            if self._packet_size <= len(self._byte_buffer):
                # Send packet to message app
                message = self._byte_buffer[2:self._packet_size].decode()
                self._callback(message)
                self._packet_size = 0
                self._byte_buffer = b''
        elif len(byte_string) > 2:
            if byte_string[0] == Packet.magic_num:
                self._packet_size = byte_string[1]
                self._byte_buffer = byte_string

    def start(self):
        self.receiver.startRealtime(self._handle_message)
        self.transmitter.start()

    def stop(self):
        self.receiver.stopRealtime()
        self.transmitter.stop()

    def send(self, msg):
        msg_bytes = msg.encode('utf-8')
        size = len(msg_bytes)
        assert size < (255 - Packet.header_size)
        packet = Packet(msg_bytes)
        self._send_bytes(packet.bytes)

#    def send(self, msg):
#        msg_bytes = msg.encode('utf-8')
#        size = len(msg_bytes)
#        data = []
#        while size > 254:
#            byte_string = bytes([0xfe]) + msg_bytes[:254]
#            data.append(byte_string)
#            msg_bytes = msg_bytes[255:]
#            size = len(msg_bytes)
#        byte_string = bytes([size]) + msg_bytes
#        data.append(byte_string)
#        for byte_string in data:
#            assert type(byte_string) == bytes
#            self._send_bytes(byte_string)

    def _send_bytes(self, byte_string: bytes):
        self.transmitter.transmit(bytes([0]))
        self.transmitter.transmit(byte_string)

class Client:
    def __init__(self, client_id: int) -> None:
        self.network = NetworkManager(client_id, self.receive_message)

    def start(self):
        self.network.start()

    def main(self):
        self.start()
        while True:
            msg = input('--> ')
            if ":q" in msg:
                break
            if len(msg) > 255 - Packet.header_size:
                print("Message too long...")
                continue
            self.send_message(msg)
        self.stop()

    def send_message(self, msg):
        self.network.send(msg)

    def receive_message(self, msg):
        print(msg)

    def stop(self):
        self.network.stop()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('client_id', type=int, choices=[1, 2],
                        help="ID of client joining the chat room")
    args = parser.parse_args()
    client = Client(args.client_id)
    client.main()
