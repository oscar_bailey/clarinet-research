import pprofile
import sys
sys.path.insert(0, '.')
import clarinet
import clarinet.read_wav as read_wav

if __name__ == "__main__":
    prof = pprofile.Profile()
    with prof():
        result = read_wav.read_wav("tests/data/everybody.wav", clarinet.FSK(2,4))
    with open("cachegrind.out.readwav", 'w') as f:
        prof.callgrind(f)
