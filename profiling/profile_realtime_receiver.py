import pprofile
import sys
sys.path.insert(0, '.')
sys.path.insert(0, 'tests')
import test_all

if __name__ == "__main__":
    prof = pprofile.Profile()
    with prof():
        test_all.test_realtime_receiver(b'Hello World')
    with open("cachegrind.out.realtime_receiver", 'w') as f:
        prof.callgrind(f)
