import pytest
import clarinet.modem as modem
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile

def setup_transmitter_receiver(network = None):
    if not network:
        modulator = modem.FSK(2, 2)
        network = modem.NetworkDescription(modulator)
    transmitter = modem.Transmitter(network)
    receiver = modem.Receiver(network)
    return transmitter, receiver

def setup_frame_and_parser(byte_string, network = None):
    if not network:
        modulator = modem.FSK(2, 2)
        network = modem.NetworkDescription(modulator)
    frame = modem._Frame(byte_string, network)
    signal = frame.getAudio()
    frame_parser = modem._FrameParser(signal, network)
    byte_string_out = frame_parser.getByteString()
    return frame, frame_parser

test_strings = [
    b'Hi', b'Hello World', b'djlsfdjsfjsldfjdslfjsljfdsjfsdf',
    b'Well I never', b'Shall I compare thee to a summer\'s day, etc. etc. etc.'
]

test_modulators = [
    modem.FSK(2,2), modem.FSK(2,4), modem.OOK(4), modem.PSK(2, 200)
]

test_strings_modulators = [
    (string, modulator) for string in test_strings for modulator in test_modulators
]

test_strings_baud = [
    (string, baud) for string in test_strings for baud in [2,4,8]
]

@pytest.mark.parametrize("byte_string,modulator", test_strings_modulators)
def test_signal_size(byte_string, modulator):
    network = modem.NetworkDescription(modulator)
    transmitter, receiver = setup_transmitter_receiver(network)
    symbol_width = transmitter.modulator.symbol_width
    frame_length_samples_total = network.frame_length_total * symbol_width
    frame_count = np.ceil(
        (len(byte_string) * transmitter.modulator.symbols_per_byte * symbol_width)
        / frame_length_samples_total)
    signal = transmitter.generateAudio(byte_string)
    assert signal.size == frame_count * frame_length_samples_total

@pytest.mark.parametrize("byte_string,modulator", test_strings_modulators)
def test_transmit_receive_integrity(byte_string, modulator):
    string_in = byte_string
    network = modem.NetworkDescription(modulator)
    transmitter, receiver = setup_transmitter_receiver(network)
    audio = transmitter.generateAudio(string_in)
    string_out = receiver.retrieveByteStringFromSignal(audio)
    assert string_in in string_out

@pytest.mark.parametrize("byte_string,modulator", test_strings_modulators)
def test_transmit_receive_equality(byte_string, modulator):
    string_in = byte_string
    network = modem.NetworkDescription(modulator)
    transmitter, receiver = setup_transmitter_receiver(network)
    audio = transmitter.generateAudio(string_in)
    string_out = receiver.retrieveByteStringFromSignal(audio)
    assert string_in == string_out

@pytest.mark.parametrize("modulator", test_modulators)
def test_frame_parsing_integrity(modulator):
    byte_string = b'Hey'
    network = modem.NetworkDescription(modulator)
    frame, frame_parser = setup_frame_and_parser(byte_string, network)
    byte_string_out = frame_parser.getByteString()
    assert byte_string in byte_string_out

@pytest.mark.parametrize("modulator", test_modulators)
def test_frame_parsing_equality(modulator):
    byte_string = b'Hey'
    network = modem.NetworkDescription(modulator)
    frame, frame_parser = setup_frame_and_parser(byte_string, network)
    byte_string_out = frame_parser.getByteString()
    assert byte_string == byte_string_out

@pytest.mark.parametrize("modulator", test_modulators)
def test_frame_symbols_eq(modulator):
    byte_string = b'Hey'
    network = modem.NetworkDescription(modulator)
    frame, frame_parser = setup_frame_and_parser(byte_string, network)
    for i in range(frame._symbols_payload.shape[0]):
        val_in = frame_parser.modulator.parseSymbol(frame._symbols_payload[i])
        val_out = frame_parser.modulator.parseSymbol(frame_parser._payload_symbols[i])
        assert val_in == val_out and i >= 0

@pytest.mark.parametrize("byte_string", test_strings)
def test_transmit_receive_symbols_eq(byte_string):
    modulator = modem.FSK(2, 2)
    network = modem.NetworkDescription(modulator)
    transmitter, receiver = setup_transmitter_receiver()
    frames_in = transmitter._generateFrames(byte_string)
    audio = transmitter.generateAudio(byte_string)
    signal_parser = modem._SignalParser(audio, network)
    frames_out = list(signal_parser.getFrames().values())
    assert len(frames_in) == len(frames_out)
    for i in range(len(frames_in)):
        symbols_in = frames_in[i].getDataSymbols()
        symbols_out = frames_out[i].getDataSymbols()
        for j in range(symbols_in.shape[0]):
            val_in = modulator.parseSymbol(symbols_in[i])
            val_out = modulator.parseSymbol(symbols_out[i])
            assert val_in == val_out

@pytest.mark.parametrize("modulator", test_modulators)
def test_symbol_composition(modulator):
    byte_string = b'Hey'
    network = modem.NetworkDescription(modulator)
    frame, frame_parser = setup_frame_and_parser(byte_string, network)
    for i in range(modulator.symbols_max_val+1):
        symbol = modulator.generateSymbol(i)
        val = modulator.parseSymbol(symbol)
        assert i == val

def test_byte_to_vals_and_back():
    for bits_per_symbol in [1, 2, 4, 8]:
        for i in range(255):
            byte_string = bytes([i])
            vals = modem.split_byte_string(byte_string, bits_per_symbol)
            composed_byte = modem.compose_byte_string(vals, bits_per_symbol)
            assert composed_byte == byte_string

@pytest.mark.parametrize("byte_string", test_strings)
def test_byte_string_to_vals_and_back(byte_string):
    modulator = modem.FSK(2, 2)

    vals = modem.split_byte_string(byte_string, modulator.bits_per_symbol)
    byte_string_out = modem.compose_byte_string(vals, modulator.bits_per_symbol)
    assert byte_string == byte_string_out

#test_audio = ["everybody-amplified.wav", "everybody.wav"]
test_audio = ["everybody.wav"]
test_audio = ['tests/data/%s' % filename for filename in test_audio]

#@pytest.mark.parametrize("filename", test_audio)
#def test_recorded_audio_integrity(filename):
#    rate, data = scipy.io.wavfile.read(filename)
#    receiver = modem.Receiver(modem.FSK(2, 4))
#    audio = receiver._reformatSignal(data, rate)
#    parser = modem._SignalParser(audio, modem.FSK(2, 4))
#    assert b'Hi everybody' in parser.getBytes()

@pytest.mark.parametrize("byte_string", test_strings)
def test_realtime_receiver(byte_string):
    modulator = modem.FSK(2,2)
    network = modem.NetworkDescription(modulator)
    transmitter, receiver = setup_transmitter_receiver(network)
    byte_string_out = b''
    def callback(x):
        nonlocal byte_string_out
        byte_string_out += x
    rt_receiver = modem._RealtimeReceiver(callback, network)
    chunk_size = rt_receiver.chunk_size
    audio = transmitter.generateAudio(byte_string)
    #audio = np.pad(audio, (0,rt_receiver.buffer_length), 'constant')
    for i in range(0, audio.shape[0], chunk_size):
        data_in = audio[i:i+chunk_size]
        data_len = data_in.shape[0]
        if data_len < chunk_size:
            data_in = np.pad(data_in, (0, chunk_size - data_len), 'constant')
        rt_receiver._streamCallback(data_in, chunk_size, None, None)
        print(i, rt_receiver.next_frame)
    assert byte_string == byte_string_out

if __name__ == "__main__":
    test_realtime_receiver(b'Hello World')
    #test_recorded_audio_integrity("everybody.wav")
    #test_transmit_receive_symbols_eq(b'Hello World')
    #test_frame_symbols_eq()
    ##test_frame_symbol_composition()
    # test_byte_string_to_vals_and_back()
