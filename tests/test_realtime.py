import time
import pytest
from clarinet import modem
from test_all import \
    setup_transmitter_receiver, setup_frame_and_parser, test_strings, \
    test_modulators, test_strings_modulators

fast_modulators = [
    modem.FSK(2,60), modem.OOM(2,60)
]

test_strings_fast_modulators = [
    (string, modulator) \
        for string in test_strings \
        for modulator in fast_modulators
]

@pytest.mark.parametrize("byte_string,modulator", test_strings_fast_modulators)
def test_send_receive_integrity(byte_string, modulator):
    transmitter, receiver = setup_transmitter_receiver(modulator)
    # Initialise transmitter, receiver
    receiver.startRealtime()
    transmitter.start()
    time.sleep(0.5)
    # Transmit the string
    transmitter.transmit(byte_string)
    while transmitter.active:
        time.sleep(0.1)
    time.sleep(2)
    # Stop the receiver
    receiver.stopRealtime()
    transmitter.stop()
    string_out = receiver.getBuffer()
    assert byte_string in string_out
