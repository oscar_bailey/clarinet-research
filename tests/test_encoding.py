import pytest
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile

from clarinet.encoding import ConvolutionCode12, ReedSolomon, Checksum

test_strings = [
    b'Hi', b'Hello World', b'djlsfdjsfjsldfjdslfjsljfdsjfsdf',
    b'Well I never', b'Shall I compare thee to a summer\'s day, etc. etc. etc.'
]


test_strings_memory = [(string, mem) for string in test_strings
                                     for mem in range(2,8)]

test_strings_codelength = [(string, length) for string in test_strings
                                         for length in range(2,12)]

test_strings_codelengthsmall = [(string, length) for string in test_strings
                                         for length in range(1,3)]

@pytest.mark.parametrize("byte_string,code_length", test_strings_codelength)
def test_reed_solomon(byte_string, code_length):
    reed_solomon = ReedSolomon(32, code_length)
    encoded_string = reed_solomon.encode(byte_string)
    success, decoded_string = reed_solomon.decode(encoded_string)
    assert byte_string == decoded_string
    assert success

@pytest.mark.parametrize("byte_string,memory", test_strings_memory)
def test_convolution_code(byte_string, memory):
    conv_code = ConvolutionCode12(32, memory)
    encoded_string = conv_code.encode(byte_string)
    success, decoded_string = conv_code.decode(encoded_string)
    assert byte_string == decoded_string
    assert success

@pytest.mark.parametrize("byte_string,code_length",
                         test_strings_codelengthsmall)
def test_convolution_code(byte_string, code_length):
    checksum = Checksum(32, code_length)
    encoded_string = checksum.encode(byte_string)
    success, decoded_string = checksum.decode(encoded_string)
    assert byte_string == decoded_string
    assert success


if __name__ == "__main__":
    test_convolution_code(b'Hi', 3)
