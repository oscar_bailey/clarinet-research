import pytest
import clarinet.modem as modem
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile

modulators = [
    modem.FSK(2,50), modem.OOK(50), modem.PSK(1, 100), modem.PSK(2, 100)
]

@pytest.mark.parametrize("modulator", modulators)
def test_generating_parsing_symbols(modulator):
    vals = list(range(modulator.symbols_max_val + 1))
    parsed_vals = []
    for val in vals:
        symbol = modulator.generateSymbol(val)
        parsed_val = modulator.parseSymbol(symbol)
        parsed_vals.append(parsed_val)
    assert vals == parsed_vals

@pytest.mark.parametrize("bits_per_symbol", [1,2])
def test_psk_phaseToVal(bits_per_symbol):
    psk = modem.PSK(bits_per_symbol, 100)
    angle_increment = 2 * np.pi / (2**bits_per_symbol)
    for i in range(bits_per_symbol):
        phase = i * angle_increment
        assert psk._phaseToVal(phase) == i
    assert psk._phaseToVal(2*np.pi) == 0

if __name__ == "__main__":
    test_generating_parsing_symbols(modem.PSK(2, 100))
