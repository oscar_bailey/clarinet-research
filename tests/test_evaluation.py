import time
import threading
import clarinet.evaluation as evaluation

def test_client_server():
    server = evaluation.Server('localhost', 0)
    host, port = server.address
    client = evaluation.Client(host, port)
    num_tests = 10
    server_thread = threading.Thread(target=server.begin, args=(num_tests))
    client_thread = threading.Thread(target=client.begin)
    server_thread.start()
    time.sleep(0.5)
    client_thread.start()
    # Wait for threads to finish
    server_thread.join()
    client_thread.join()
