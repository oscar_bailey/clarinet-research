import clarinet.modem as modem
import sys

def read_wav(filename, modulator):
    receiver = modem.Receiver(modulator)
    byte_string = receiver.retrieveByteStringFromWav(filename)
    print("%s" % byte_string)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Read contents of a wav file\n\nUsage: %s filename.wav" % sys.argv[0])
        sys.exit(-1)

    read_wav(sys.argv[1], modem.FSK(2,2))
