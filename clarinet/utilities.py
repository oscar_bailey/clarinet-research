from functools import reduce
import numpy as np

def pad_to_nextpow2(arr_in: np.ndarray) -> np.ndarray:
    n = arr_in.shape[0]
    y = np.floor(np.log2(n))
    nextpow2 = int(np.power(2, y+1))
    padded_arr = np.pad(arr_in, (0, nextpow2-n), mode='constant')
    return padded_arr

def get_bit_mask(start: int, end: int) -> int:
    return ((1 << (end-start))-1) << start

def compose_byte_string(vals: np.ndarray, bits_per_symbol: int) -> bytes:
    assert (8.0 / bits_per_symbol).is_integer()
    assert bits_per_symbol <= 8 and bits_per_symbol > 0
    vals_per_byte = 8 // bits_per_symbol
    assert (float(vals.size) / vals_per_byte).is_integer()
    byte_count = vals.size // vals_per_byte
    byte_string = b''
    for i in range(byte_count):
        cur_vals = [vals[i*vals_per_byte + j] << j * bits_per_symbol\
            for j in range(vals_per_byte)]
        byte_string += bytes([reduce(lambda x, y: x | y, cur_vals)])
    return byte_string

def split_byte_string(byte_string: bytes, bits_per_symbol: int) -> np.ndarray:
    assert (8.0 / bits_per_symbol).is_integer()
    assert bits_per_symbol <= 8 and bits_per_symbol > 0
    byte_string_bit_length = len(byte_string) * 8
    vals = np.zeros((byte_string_bit_length // bits_per_symbol,)).astype(np.int)
    for bit_index in range(0, byte_string_bit_length, bits_per_symbol):
        byte_index = bit_index // 8
        start = bit_index % 8
        end = start + bits_per_symbol
        mask = get_bit_mask(start, end)
        vals[bit_index // bits_per_symbol] = \
            (byte_string[byte_index] & mask) >> start
    return vals

def random_bytes(length: int):
    return open("/dev/urandom","rb").read(length)
