import numpy as np
from commpy.channelcoding import Trellis, conv_encode, viterbi_decode
from reedsolo import RSCodec, ReedSolomonError
from clarinet.utilities import compose_byte_string, split_byte_string

# Taken from:
# New rate 1/2, 1/3, and 1/4 binary convolutional encoders with an optimum
#   distance profile
# R. Johannesson et al. (1999)
GENERATOR_MATRICES = {
    2: [0o7, 0o5],
    3: [0o74, 0o54],
    4: [0o62, 0o56],
    5: [0o77, 0o45],
    6: [0o634, 0o564],
    7: [0o626, 0o572]
}

class Encoding:
    def __init__(self):
        self.max_size = None

    def encode(self, bytes_) -> bytes:
        pass

    def decode(self, bytes_) -> (bool, bytes):
        pass

class NoEncoding(Encoding):
    def __init__(self, frame_size):
        self.max_size = frame_size

    def encode(self, byte_string):
        return byte_string

    def decode(self, byte_string):
        return (True, byte_string)

class ConvolutionCode12(Encoding):
    def __init__(self, frame_size, memory):
        # Calculate max_size and set attributes
        frame_size_bits = frame_size * 8
        max_bits = (frame_size_bits + memory) * 2
        padding = 8 - (max_bits % 8)
        self.max_size = int((max_bits + padding) / 8)
        self.memory = memory
        self.gen_matrix = np.array([GENERATOR_MATRICES[memory]])
        self.trellis = Trellis(np.array([memory]), self.gen_matrix)

    def encode(self, message_bytes):
        message_bits = split_byte_string(message_bytes, 1)
        encoded_bits = conv_encode(message_bits, self.trellis).astype(int)
        # Pad bits to the nearest byte
        padding = np.zeros((8 - (encoded_bits.size % 8),), dtype=int)
        encoded_bits = np.concatenate((encoded_bits, padding))
        encoded_bytes = compose_byte_string(encoded_bits, 1)
        return encoded_bytes

    def decode(self, encoded_bytes):
        encoded_bits = split_byte_string(encoded_bytes, 1)
        # Undo padding
        unpadded_size = int(((encoded_bits.size / 2) - self.memory) // 8) * 8
        encoded_size = (unpadded_size + self.memory) * 2
        encoded_bits = encoded_bits[:encoded_size]
        message_bits = viterbi_decode(encoded_bits, self.trellis).astype(int)
        message_bytes = compose_byte_string(message_bits[:-self.memory], 1)
        return True, message_bytes

class ReedSolomon(Encoding):
    def __init__(self, frame_size, code_length):
        self.max_size = frame_size + code_length
        self.rs_codec = RSCodec(code_length)
        self.code_length = code_length

    def encode(self, message_bytes):
        return self.rs_codec.encode(message_bytes)

    def decode(self, encoded_bytes):
        success = True
        try:
            decoded_bytes = self.rs_codec.decode(encoded_bytes)
        except ReedSolomonError:
            success = False
            decoded_bytes = encoded_bytes[:-self.code_length]
        return success, decoded_bytes

class Checksum(Encoding):
    def __init__(self, frame_size, code_length=1):
        self.code_length = code_length
        self.max_size = frame_size + code_length

    def _getCheckBytes(self, message_bytes):
        check = sum(message_bytes) % (2**(8*self.code_length))
        check_vals = []
        for i in range(self.code_length):
            current_val = (check >> (8*i)) & 255
            check_vals.append(current_val)
        check_bytes = bytes(check_vals)
        return check_bytes

    def encode(self, message_bytes):
        check_bytes = self._getCheckBytes(message_bytes)
        return message_bytes + check_bytes

    def decode(self, encoded_bytes):
        check_bytes = encoded_bytes[-self.code_length:]
        message_bytes = encoded_bytes[:-self.code_length]
        success = (self._getCheckBytes(message_bytes) == check_bytes)
        return success, message_bytes

