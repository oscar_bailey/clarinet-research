#MIT License
#
#Copyright (c) 2018 Oscar Bailey
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

import sys
import time
import socket
from threading import Lock
from functools import reduce
import traceback
import dill
import pyaudio
import numpy as np
from clarinet.modem import \
    DEFAULT_SAMPLE_RATE, FSK, Transmitter, Receiver, _SignalParser,\
    split_byte_string, NetworkDescription

# Object returns empty string as every attribute
# Used if colorama is not present
class _EmptyString: # pylint: disable=too-few-public-methods
    def __getattr__(self, name):
        return ''

try:
    import colorama # For colored terminal output
    from colorama import Fore, Back, Style
    colorama.init(autoreset=True)
except ImportError:
    Fore = _EmptyString()
    Back = _EmptyString()
    Style = _EmptyString()

class SocketCommsException(Exception):
    pass

class SocketManager:
    command_length = 50
    bufsize = 4096

    def __init__(self, socket_, debug=False):
        self._buffer = b''
        self._socket = socket_
        self._debug = debug

    def _createCommand(self, command_string):
        byte_string = command_string.encode()
        if len(byte_string) > self.command_length:
            msg = "Command string too long, should be <= %d, actual length %d:\n %s"\
                  % (self.command_length, len(byte_string), command_string)
            raise Exception(msg)
        # Pad command
        padding = self.command_length - len(byte_string)
        pad_bytes = padding * b'\0'
        return byte_string + pad_bytes

    def sendCommand(self, command_string):
        byte_string = self._createCommand(command_string)
        self.sendData(byte_string)
        if self._debug:
            print("SendCommand:\t%s" % command_string)

    def sendData(self, byte_string):
        length = len(byte_string)
        total_bytes_sent = 0
        while total_bytes_sent < length:
            bytes_sent = self._socket.send(byte_string[total_bytes_sent:])
            total_bytes_sent += bytes_sent

    def _parseCommand(self, command_bytes):
        try:
            end = command_bytes.index(b'\0')
        except ValueError:
            end = self.command_length
        return command_bytes[:end]

    def recvCommand(self):
        command_bytes = self.recvData(self.command_length)
        command = self._parseCommand(command_bytes)
        if self._debug:
            print("RecvCommand:\t%s" % command)
        return command

    def recvData(self, num_bytes):
        while len(self._buffer) < num_bytes:
            new_bytes = self._socket.recv(self.bufsize)
            self._buffer = self._buffer + new_bytes
        data = self._buffer[:num_bytes]
        self._buffer = self._buffer[num_bytes:]
        return data

class Client:
    def __init__(self, host, port, debug=False):
        self._soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._address = (host, port)
        self._debug = debug

    @property
    def address(self):
        return self._address

    def begin(self):
        self._soc.connect(self._address)
        socket_manager = SocketManager(self._soc, debug=self._debug)
        try:
            self._begin(socket_manager)
        except SocketCommsException:
            traceback.print_exc()
        finally:
            self._soc.close()

    def _begin(self, socket_manager):
        print("Connected to host, starting...")
        # RecvCommand: "TESTS x" (where x is the number of trials)
        command = socket_manager.recvCommand()
        command_split = command.split()
        if len(command_split) != 2:
            raise SocketCommsException("Unexpected command: %s" % command)
        num_tests = int(command_split[1])
        recorded_audio = []
        recorder = Recorder()
        for i in range(num_tests):
            # RecvCommand: "START"
            command = socket_manager.recvCommand()
            if not b'START' in command:
                raise SocketCommsException("Unexpected command: %s" % command)
            recorder.start(wipe_audio=True)
            # RecvCommand: "STOP"
            command = socket_manager.recvCommand()
            if not b'STOP' in command:
                raise SocketCommsException("Unexpected command: %s" % command)
            recorder.stop()
            recorded_audio.append(recorder.recorded_audio.tostring())
        # Send recorded data
        for i, audio_data in enumerate(recorded_audio):
            length = len(audio_data)
            socket_manager.sendCommand("TRIAL %d LENGTH %d" % (i, length))
            socket_manager.sendData(audio_data)

class Server:
    def __init__(self, host='localhost', port=0, debug=False):
        self._soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._address = (host, port)
        self._soc.bind(self._address)
        self._debug = debug

    @property
    def address(self):
        return self._soc.getsockname()

    def begin(self, test_batch):
        print("Listening on %s:%d" % self.address)
        self._soc.listen(1)
        (clientsocket, _) = self._soc.accept()
        socket_manager = SocketManager(clientsocket, debug=self._debug)
        try:
            self._begin(test_batch, socket_manager)
        except SocketCommsException:
            traceback.print_exc()
        finally:
            clientsocket.close()
            self._soc.close()

    def _begin(self, test_batch, socket_manager):
        # Send number of tests to client
        socket_manager.sendCommand("TESTS %d" % test_batch.size)
        # Start Testing
        for test in test_batch:
            for i in range(test.repeats):
                socket_manager.sendCommand("START")
                test.transmit(i)
                socket_manager.sendCommand("STOP")
        # Stop Testing
        # Receive audio data from client
        for _ in range(test_batch.size):
            # RecvCommand: "TRIAL x LENGTH y" (where x is the trial index
            # and y is the bytes-length of data to be received)
            command = socket_manager.recvCommand()
            command_split = command.split()
            if len(command_split) != 4:
                raise SocketCommsException("Unexpected command: %s" % command)
            trial_num = int(command_split[1])
            data_length = int(command_split[3])
            data = socket_manager.recvData(data_length)
            test_batch.saveTrial(trial_num, data)

class Test:
    def __init__(self, network, repeats, data):
        self._network = network
        self._repeats = repeats
        self._data = data
        self._recorded_audio = [b'' for _ in range(repeats)]

    def transmit(self, trial_num):
        # Get the transmission data
        if type(self._data) is list:
            data = self._data[trial_num]
        else:
            data = self._data
        with Transmitter(self.network) as transmitter:
            time.sleep(0.5)
            transmit_time = transmitter.transmit(data)
            time.sleep(transmit_time + 0.5)

    def saveAudio(self, trial_num, audio_data):
        self._recorded_audio[trial_num] = audio_data

    def getAudio(self, index):
        return np.fromstring(self._recorded_audio[index], dtype=np.float32)

    @property
    def complete(self):
        # Has the test been carried out?
        complete = reduce(lambda x, y: bool(x) and bool(y), self._recorded_audio)
        return complete

    @property
    def network(self):
        return self._network

    @property
    def repeats(self):
        return self._repeats

    @property
    def data(self):
        return self._data

class TestBatch:
    """ Describes a sequence of tests """
    def __init__(self, speaker, microphone, distance, environment,
                 description="", default_repeats=3,
                 default_data=b'Hello World'):
        # Settings
        self._default_repeats = default_repeats
        self._default_data = default_data
        # Lists
        self._tests = []
        # Description
        self.speaker = speaker
        self.microphone = microphone
        self.distance = distance
        self.environment = environment
        self.description = description

    def addTest(self, network, repeats=-1, data=None):
        if repeats < 0:
            repeats = self._default_repeats
        if not data:
            data = self._default_data
        new_test = Test(network, repeats, data)
        self._tests.append(new_test)

    def saveTrial(self, trial_num, audio_data):
        total = 0
        for test in self._tests:
            for i in range(test.repeats):
                if trial_num == total:
                    test.saveAudio(i, audio_data)
                    return
                total += 1

    def saveToFile(self, filename):
        with open(filename, 'wb') as file_:
            dill.dump(self, file_)

    @property
    def tests(self):
        return self._tests

    @property
    def size(self):
        return sum([test.repeats for test in self._tests])

    def __len__(self):
        return len(self._tests)

    def __iter__(self):
        return self._tests.__iter__()

    def __getitem__(self, index):
        # val is either an int index, or a slice
        new_test_batch = TestBatch(
            self.speaker, self.microphone, self.distance, self.environment,
            self._default_repeats, self._default_data
        )
        # Make sure to always get a list of tests
        list_type = type(self._tests)
        tests = self._tests[index]
        if type(tests) != list_type:
            tests = list_type([tests])
        new_test_batch._tests = tests
        return new_test_batch

    def __repr__(self):
        msg = Fore.GREEN + "Hardware\n" + Style.RESET_ALL
        msg += "\tSpeaker: %s\n" % self.speaker
        msg += "\tMicrophone: %s\n" % self.microphone
        msg += Fore.GREEN + "Environment\n" + Style.RESET_ALL
        msg += "\t\"%s\"\n" % self.environment
        msg += Fore.GREEN + "Description\n" + Style.RESET_ALL
        msg += "\t\"%s\"\n" % self.description
        msg += "\tRange: %.2fm\n" % self.distance
        msg += Fore.GREEN + "Tests defined:  %d\n" % len(self._tests)
        msg += Fore.GREEN + "Tests recorded: %d" \
            % sum([int(test.complete) for test in self._tests])
        return msg

class Recorder:
    def __init__(self, sample_rate=DEFAULT_SAMPLE_RATE):
        self.recorded_audio = np.zeros((1,), dtype=np.float32)
        self._flag_lock = Lock()
        self._flag = pyaudio.paContinue
        self._py_audio = None
        self._stream = None
        self._sample_rate = sample_rate

    def _streamCallback(self, in_data, frame_count, time_info, status): # pylint: disable=unused-argument
        audio_segment = np.fromstring(in_data, dtype=np.float32)
        if audio_segment.size > 0:
            self.recorded_audio = np.concatenate((self.recorded_audio, audio_segment))
        return (None, self._flag)

    def start(self, wipe_audio=False):
        if wipe_audio:
            self.recorded_audio = np.zeros((1,), dtype=np.float32)
        with self._flag_lock:
            self._flag = pyaudio.paContinue
        self._py_audio = pyaudio.PyAudio()
        self._stream = self._py_audio.open(
            format=pyaudio.paFloat32,
            channels=1,
            rate=self._sample_rate, # TODO: Send sample rate to client
            input=True,
            stream_callback=self._streamCallback
        )

    def stop(self):
        with self._flag_lock:
            self._flag = pyaudio.paComplete
        # Wait for stream to die
        while self._stream.is_active():
            time.sleep(0.1)
        # Close stream and pyaudio
        self._stream.stop_stream()
        self._stream.close()
        self._py_audio.terminate()

class TrialEvaluator:
    def __init__(self, network, data, audio, trial_index=0):
        self.id = trial_index
        self.network = network
        self.data = data
        self.audio = audio
        self.frames = []
        self.parsed_frames = []
        self.parsed_data = None
        self.signal_parser = None
        # Initialise frame lists
        self._getFrames()
        # Define memoized attributes
        self._byte_loss = None
        self._bit_loss = None
        self._bytes_transmitted = None
        self._decoding_errors_detected = None
        self._decoding_false_positives = None
        self._decoding_frames_corrected = None
        self._decoding_frames_incorrected = None
        self._frame_loss = None
        self._signal_duration = None
        self._symbol_loss = None
        self._total_bytes = None
        self._total_symbols = None

    def alignFrames(self, frames, parsed_frames):
        distances = []
        for parsed_frame in parsed_frames:
            dist = self._hamming(frames[0], parsed_frame)
            distances.append(dist)
        # Check if there are no parsed_frames
        if not distances:
            return frames, parsed_frames
        min_dist = min(distances)
        start_index = distances.index(min_dist)
        parsed_frames = parsed_frames[start_index:]
        return frames, parsed_frames

    @staticmethod
    def _hamming(frame_a, frame_b):
        symbols_a = frame_a.getDataSymbols()
        vals_a = [
            frame_a.modulator.parseSymbol(symbol)
            for symbol in symbols_a
        ]
        symbols_b = frame_b.getDataSymbols()
        vals_b = [
            frame_b.modulator.parseSymbol(symbol)
            for symbol in symbols_b
        ]
        diff = [abs(vals_a[i] - vals_b[i]) for i in range(len(vals_a))]
        return diff

    def _getFrames(self):
        self.network.amplify = True
        transmitter = Transmitter(self.network)
        frames = transmitter._generateFrames(self.data)
        self.signal_parser = _SignalParser(self.audio, self.network)
        parsed_frames = list(self.signal_parser.getFrames().values())
        self.parsed_data = self.signal_parser.getBytes()
        frames, parsed_frames = self.alignFrames(frames, parsed_frames)
        self.frames = frames
        self.parsed_frames = parsed_frames

    def getFrameByteLoss(self, index):
        byte_loss = 0
        frame = self.frames[index]
        try:
            parsed_frame = self.parsed_frames[index]
        except IndexError:
            # Whole frame missing
            byte_loss = len(frame.byte_string)
            return byte_loss
        for i, byte in enumerate(frame.byte_string):
            try:
                byte_loss += int(byte != parsed_frame.byte_string[i])
            except IndexError:
                byte_loss += 1
        return byte_loss

    def getFrameSymbolLoss(self, index):
        frame = self.frames[index]
        symbol_loss = 0
        try:
            parsed_frame = self.parsed_frames[index]
        except IndexError:
            # Whole frame missing
            symbol_loss = self.network.frame_length_payload
            return symbol_loss
        hamming = np.array(self._hamming(frame, parsed_frame))
        symbol_loss = np.sum(hamming.astype(bool))
        return symbol_loss

    @property
    def decoding_frames_corrected(self):
        if not (self._decoding_frames_corrected is None):
            return self._decoding_frames_corrected
        frames_corrected = 0
        for i, _ in enumerate(self.frames):
            symbol_error = bool(self.getFrameSymbolLoss(i))
            byte_error = bool(self.getFrameByteLoss(i))
            frames_corrected += int(symbol_error and not byte_error)
        self._decoding_frames_corrected = frames_corrected
        return frames_corrected

    @property
    def decoding_frames_incorrected(self):
        if not (self._decoding_frames_incorrected is None):
            return self._decoding_frames_incorrected
        """ Should not happen, unless decoder has a bug """
        frames_incorrected = 0
        for i, _ in enumerate(self.frames):
            symbol_error = bool(self.getFrameSymbolLoss(i))
            byte_error = bool(self.getFrameByteLoss(i))
            frames_incorrected += int(byte_error and not symbol_error)
        self._decoding_frames_incorrected = frames_incorrected
        return frames_incorrected

    @property
    def decoding_errors_detected(self):
        if not (self._decoding_errors_detected is None):
            return self._decoding_errors_detected
        errors_detected = 0
        for _, parsed_frame in enumerate(self.parsed_frames):
            errors_detected += int(parsed_frame.getDecodingSuccessful())
        self._decoding_errors_detected = errors_detected
        return errors_detected

    @property
    def decoding_false_positives(self):
        if not (self._decoding_false_positives is None):
            return self._decoding_false_positives
        """ Decoder detected an error, but there was no error in the message
        received """
        false_positives = 0
        for i, parsed_frame in enumerate(self.parsed_frames):
            byte_error = bool(self.getFrameByteLoss(i))
            error_detected = parsed_frame.getDecodingSuccessful()
            false_positives += int(error_detected and not byte_error)
        self._decoding_false_positives = false_positives
        return false_positives

    @property
    def total_symbols(self):
        if not (self._total_symbols is None):
            return self._total_symbols
        total = (len(self.frames) - 1) * self.network.frame_length_payload
        total += self.frames[-1].payload_length
        self._total_symbols = total
        return total

    @property
    def total_bytes(self):
        if not (self._total_bytes is None):
            return self._total_bytes
        total = (len(self.frames) - 1) * self.network.frame_byte_capacity
        total += len(self.frames[-1].byte_string)
        self._total_bytes = total
        return total

    @property
    def total_bits(self):
        return 8 * self.total_bytes

    @property
    def symbol_loss(self):
        if not (self._symbol_loss is None):
            return self._symbol_loss
        symbol_loss = 0
        for i, frame in enumerate(self.frames):
            try:
                parsed_frame = self.parsed_frames[i]
            except IndexError:
                symbol_loss += self.network.frame_length_payload
                continue
            hamming = np.array(self._hamming(frame, parsed_frame))
            symbol_loss += np.sum(hamming.astype(bool))
        self._symbol_loss = symbol_loss
        return symbol_loss

    @property
    def bit_loss(self):
        if not (self._bit_loss is None):
            return self._bit_loss
        bit_loss = 0
        for i, frame in enumerate(self.frames):
            try:
                parsed_frame = self.parsed_frames[i]
            except IndexError:
                bit_loss += len(frame.byte_string) * 8
                continue
            for j, byte in enumerate(frame.byte_string):
                try:
                    parsed_byte = parsed_frame.byte_string[j]
                except IndexError:
                    bit_loss += 8
                    continue
                bit_loss += bin(byte ^ parsed_byte).count("1")
        self._bit_loss = bit_loss
        return bit_loss

    @property
    def byte_loss(self):
        if not (self._byte_loss is None):
            return self._byte_loss
        byte_loss = 0
        for i, frame in enumerate(self.frames):
            try:
                parsed_frame = self.parsed_frames[i]
            except IndexError:
                byte_loss += len(frame.byte_string)
                continue
            for j, byte in enumerate(frame.byte_string):
                try:
                    byte_loss += int(byte != parsed_frame.byte_string[j])
                except IndexError:
                    byte_loss += 1
        self._byte_loss = byte_loss
        return byte_loss

    @property
    def frame_loss(self):
        if not (self._frame_loss is None):
            return self._frame_loss
        frame_loss = 0
        for i, frame in enumerate(self.frames):
            try:
                parsed_frame = self.parsed_frames[i]
            except IndexError:
                frame_loss += 1
                continue
            frame_loss += int(frame.byte_string != parsed_frame.byte_string)
        self._frame_loss = frame_loss
        return frame_loss

    @property
    def signal_duration(self):
        if not (self._signal_duration is None):
            return self._signal_duration
        signal_duration = 0
        for frame in self.frames:
            audio = frame.getAudio()
            frame_duration = np.trim_zeros(audio).shape[0] \
                             / self.network.sample_rate
            signal_duration += frame_duration
        self._signal_duration = signal_duration
        return signal_duration

    @property
    def bytes_transmitted(self):
        if not (self._bytes_transmitted is None):
            return self._bytes_transmitted
        bytes_transmitted = 0
        for i, frame in enumerate(self.frames):
            try:
                parsed_frame = self.parsed_frames[i]
            except IndexError:
                continue
            if not self.getFrameByteLoss(i):
                bytes_transmitted += len(frame.byte_string)
        self._bytes_transmitted = bytes_transmitted
        return bytes_transmitted

class TestEvaluator:
    """ Evaluator for Test instances """
    def __init__(self, test):
        self.test = test
        self.trial_evaluators = []
        for i in range(test.repeats):
            data = test.data
            if type(test.data) is list:
                data = test.data[i]
            audio = test.getAudio(i)
            trial_eval = TrialEvaluator(test.network, data, audio,
                                        trial_index=i)
            self.trial_evaluators.append(trial_eval)

    def __getattr__(self, attr):
        try:
            count = 0
            for evaluator in self.trial_evaluators:
                count += getattr(evaluator, attr)
            return count
        except AttributeError:
            raise AttributeError("'%s' has no attribute '%s'" % \
                                 (self.__name__, attr))

class TestBatchEvaluator:
    """ Evaluator for TestBatch instances """
    def __init__(self, test_batch):
        self.test_batch = test_batch
        self.test_evaluators = []
        for test in test_batch:
            evaluator = TestEvaluator(test)
            self.test_evaluators.append(evaluator)

    def __getattr__(self, attr):
        try:
            count = 0
            for evaluator in self.test_evaluators:
                count += getattr(evaluator, attr)
            return count
        except AttributeError:
            raise AttributeError("'%s' has no attribute '%s'" % \
                                 (self.__name__, attr))

    def __repr__(self):
        msg = "Tests: " + Fore.GREEN + "%d\n" % len(self.test_evaluators)
        msg += Style.RESET_ALL
        for i, test in enumerate(self.test_evaluators):
            throughput = test.bytes_transmitted / test.signal_duration
            msg += Fore.BLUE + "Test %d\n" % (i+1) + Style.RESET_ALL
            msg += "Throughput: %.2f (Bytes/Second)\n" % throughput
            msg += "Byte loss: %d/%d (Errors/Total)\n" \
                    % (test.byte_loss, test.total_bytes)
        avg_throughput = self.bytes_transmitted / self.signal_duration
        msg += "Average throughput: %.2f (Bytes/Second)" % avg_throughput
        return msg

class Evaluator:
    def __init__(self):
        self._recorder = Recorder()

    def _getAudio(self, transmitter, byte_string):
        self._recorder.start(True)
        time.sleep(0.5)
        transmitter.start()
        transmit_time = transmitter.transmit(byte_string)
        time.sleep(transmit_time + 0.5)
        self._recorder.stop()
        transmitter.stop()
        return self._recorder.recorded_audio

    def _hamming(self, frame_a, frame_b):
        symbols_a = frame_a.getDataSymbols()
        vals_a = [
            frame_a.modulator.parseSymbol(symbol)
            for symbol in symbols_a
        ]
        symbols_b = frame_b.getDataSymbols()
        vals_b = [
            frame_b.modulator.parseSymbol(symbol)
            for symbol in symbols_b
        ]
        diff = [abs(vals_a[i] - vals_b[i]) for i in range(len(vals_a))]
        return sum(diff)

    def alignFrames(self, frames, parsed_frames):
        distances = []
        for _, parsed_frame in enumerate(parsed_frames):
            dist = self._hamming(frames[0], parsed_frame)
            distances.append(dist)
        min_dist = min(distances)
        start_index = distances.index(min_dist)
        parsed_frames = parsed_frames[start_index:]
        return frames, parsed_frames

    def _printHamming(self, frame, parsed_frame) -> float:
        symbols_in = frame.getDataSymbols()
        vals_orig = split_byte_string(
            frame.byte_string, frame.modulator.bits_per_symbol
        )
        vals_in = [frame.modulator.parseSymbol(symbol)
                   for symbol in symbols_in]
        symbols_out = parsed_frame.getDataSymbols()
        vals_out = [parsed_frame.modulator.parseSymbol(symbol)
                    for symbol in symbols_out]
        #for val in vals_orig:
        #    if val < 0:
        #        msg = 't'
        #    else:
        #        msg = str(val)
        #    print("%s" % msg, end=',')
        #print()
        for i, val in enumerate(vals_in):
            if val < 0:
                msg = 't'
            else:
                msg = str(val)
            if i < len(vals_orig):
                if val != vals_orig[i]:
                    msg = Fore.YELLOW + msg
            print("%s" % msg, end=',')
        print()
        for val in vals_out:
            if val < 0:
                msg = 't'
            else:
                msg = str(val)
            print("%s" % msg, end=',')
        print()
        total_error = 0
        for i, val in enumerate(vals_in):
            if i > len(vals_orig):
                msg = '-'
            elif vals_in[i] == vals_out[i]:
                msg = '.'
            else:
                msg = Fore.RED + 'x'
                total_error += 1
            print(msg, end=' ')
        error_percent = 100 * (total_error / len(vals_in))
        print("\nPercentage error:%s %.2f%%" % (Fore.BLUE, error_percent))
        return error_percent

    def evaluate(self, modulator, byte_string):
        network = NetworkDescription(modulator)
        transmitter = Transmitter(network)
        audio = self._getAudio(transmitter, byte_string)
        return self.evaluateAudio(audio, network, byte_string)

    def evaluateAudio(self, audio, network, byte_string):
        transmitter = Transmitter(network)
        frames = transmitter._generateFrames(byte_string)
        signal_parser = _SignalParser(audio, network)
        parsed_frames = list(signal_parser.getFrames().values())
        frames, parsed_frames = self.alignFrames(frames, parsed_frames)
        error_total = 0
        for i in range(len(frames)):
            if len(parsed_frames) > i:
                print(Fore.GREEN + "--- Frame %d ---" % i)
                error_total += self._printHamming(frames[i], parsed_frames[i])
        error_average = error_total / len(parsed_frames)
        print(Fore.GREEN + "--- Summary ---")
        print(Fore.BLUE + "Average error: %s%.2f%%" % (Fore.RESET, error_average))
        try:
            message_string = signal_parser.getBytes().decode()
            print(Fore.BLUE + "Received message: %s%s" % (Fore.RESET, message_string))
        except UnicodeDecodeError:
            message_string = "Could not decode received message, data corrupted!"
            print(Fore.BLUE + "Could not decode received message, data corrupted!")
        return message_string, error_average

def load_tests(filename):
    try:
        with open(filename, 'rb') as file_:
            test_batch = dill.load(file_)
        assert test_batch.__class__.__name__ == 'TestBatch'
    except dill.UnpicklingError:
        traceback.print_exc()
        print("Error loading tests from file")
        return None
    except EOFError:
        traceback.print_exc()
        print("Check filename specfied is correct...")
        return None
    return test_batch
