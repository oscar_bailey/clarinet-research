import clarinet.modem as modem
import sys
import time

def transmit(baud, filename):
    with open(filename, 'rb') as file_:
        raw_data = file_.read()
    modulator = modem.FSK(2, baud)
    network = modem.NetworkDescription(modulator)
    with modem.Transmitter(network) as transmitter:
        time.sleep(0.5)
        transmit_time = transmitter.transmit(raw_data)
        print("Transmit Time: %f" % transmit_time)
        time.sleep(transmit_time + 0.5)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Transmit a binary file.\n\nUsage: %s <baud> filename.bin" % sys.argv[0])
        sys.exit(-1)
    try:
        baud = int(sys.argv[1])
    except ValueError:
        print("baud should be an integer")
        sys.exit(-1)

    transmit(baud, sys.argv[2])
