import clarinet.modem as modem
import time
import sys

def print_flush(string):
    print("%s" % string, flush=True, end='')

def listen(baud, sleep_time):
    modulator = modem.FSK(2, baud)
    network = modem.NetworkDescription(modulator)
    realtime_receiver = modem._RealtimeReceiver(print_flush, network)
    realtime_receiver.start()
    # Sleep
    time.sleep(sleep_time)
    realtime_receiver.stop()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Listen for incoming messages.\n\nUsage: %s <baud> <sleep_time>" % sys.argv[0])
        sys.exit(-1)

    try:
        baud = int(sys.argv[1])
    except ValueError:
        print("baud should be an integer")
        sys.exit(-1)

    try:
        sleep_time = float(sys.argv[2])
    except ValueError:
        print("sleep_time should be a float")
        sys.exit(-1)

    listen(baud, sleep_time)
