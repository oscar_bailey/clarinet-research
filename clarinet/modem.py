#MIT License
#
#Copyright (c) 2018 Oscar Bailey
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

import time
import wave
import sys
from functools import reduce
from threading import Lock
import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile
import scipy.signal
from typing import List, Dict, Type # pylint: disable=unused-import

from clarinet.__version__ import __version__
from clarinet.utilities import pad_to_nextpow2, get_bit_mask,\
    compose_byte_string, split_byte_string, random_bytes
from clarinet.encoding import Encoding, ConvolutionCode12, ReedSolomon, NoEncoding

DEFAULT_SAMPLE_RATE = 44100

class Modulation:
    def __init__(self, bits_per_symbol: int, baud: int, smoothing: bool = True,
                 gray_code: bool = True,
                 sample_rate: int = DEFAULT_SAMPLE_RATE) -> None:
        self.baud: int = -1
        self.sample_rate: int = -1
        self.symbol_width: int = -1
        self.bits_per_symbol: int = -1
        self.symbols_per_byte: int = -1
        self.symbols_max_val: int = -1
        self.smoothing = smoothing
        self.gray_code = gray_code
        # Initialise the attributes above
        self.initialise(bits_per_symbol, baud, sample_rate)
        self.sync_symbol: np.ndarray = np.array([], dtype=np.float32)
        self.termination_symbol: np.ndarray = np.array([], dtype=np.float32)

    def initialise(self, bits_per_symbol: int, baud: int, sample_rate: int) -> None:
        self.baud = baud
        self.sample_rate = sample_rate
        self.symbol_width = sample_rate // baud
        symbols_per_byte = 8.0 / bits_per_symbol
        assert symbols_per_byte.is_integer()
        self.bits_per_symbol = bits_per_symbol
        self.symbols_per_byte = int(symbols_per_byte)
        self.symbols_max_val = (1 << self.bits_per_symbol) - 1

    @staticmethod
    def smoothSignal(signal: np.ndarray) -> np.ndarray:
        std = (signal.size / 2) * 0.7
        window = scipy.signal.gaussian(signal.size, std)
        return signal * window

    @staticmethod
    def binaryToGray(num: int) -> int:
        return num ^ (num >> 1)

    @staticmethod
    def grayToBinary(num: int) -> int:
        if num < 0:
            return num
        mask = num
        while mask != 0:
            mask = mask >> 1
            num = num ^ mask
        return num

    def generateSignalRaw(self, freq: float, phase: float, num_samples: int) -> np.ndarray:
        duration = num_samples / self.sample_rate
        time_ = freq * 2.0 * np.pi * np.linspace(0, duration, np.floor(num_samples))
        return np.sin(time_ + phase)

    def generateSymbol(self, val: int) -> np.ndarray: # pylint: disable=unused-argument
        return np.array([])

    def parseSymbol(self, symbol: np.ndarray) -> int: # pylint: disable=unused-argument
        return -1

    def __repr__(self):
        return self.__class__.__name__

class NetworkDescription:
    def __init__(self, modulator: Modulation, frame_length_header: int = 1,
            frame_byte_capacity: int = 32, encoding: Encoding = None,
            amplify: bool = False) -> None:
        self.baud = -1
        self.sample_rate = -1
        self._modulator = None
        self.modulator = modulator
        self.amplify = amplify
        self.encoding = encoding
        if not encoding:
            self.encoding = NoEncoding(frame_byte_capacity)
        self.frame_byte_capacity = frame_byte_capacity
        self.frame_length_payload = self.encoding.max_size * modulator.symbols_per_byte
        self.frame_length_header = frame_length_header
        self.frame_length_total = self.frame_length_payload + frame_length_header

    @property
    def modulator(self):
        return self._modulator

    @modulator.setter
    def modulator(self, new_modulator):
        self._modulator = new_modulator
        self.baud = new_modulator.baud
        self.sample_rate = new_modulator.sample_rate

class FSK(Modulation):
    def __init__(self, bits_per_symbol: int, baud: int, smoothing: bool = True,
                 gray_code: bool = True,
                 sample_rate: int = DEFAULT_SAMPLE_RATE) -> None:
        super(FSK, self).__init__(bits_per_symbol, baud, smoothing, gray_code,
                                  sample_rate)
        # Declare variables
        self.sync_symbol = None
        self.termination_symbol = None
        self._sync_freq = 0
        self._termination_freq = 0
        # Initialise variables
        self._base_freq = 1000 #Hz
        self.sync_freq = int(self.base_freq)
        self.termination_freq = int(self.base_freq + 300)
        self.value_frequencies = [self._base_freq + (700 * (i+1)) \
            for i in range(self.symbols_max_val + 1)]

    @property
    def sync_freq(self):
        return self._sync_freq

    @sync_freq.setter
    def sync_freq(self, freq):
        self._sync_freq = freq
        self.sync_symbol = self.generateSignalRaw(freq, 0, self.symbol_width)

    @property
    def termination_freq(self):
        return self._termination_freq

    @termination_freq.setter
    def termination_freq(self, freq):
        self._termination_freq = freq
        self.termination_symbol = self.generateSignalRaw(freq, 0, self.symbol_width)

    @property
    def base_freq(self):
        return self._base_freq

    @base_freq.setter
    def base_freq(self, value: int):
        self._base_freq = value
        self.sync_freq = int(value * 1.5)
        self.termination_freq = int(value * 2.5)

    def generateSymbol(self, val: int) -> np.ndarray:
        assert val >= 0 and val <= self.symbols_max_val, "Val is out of range"
        if self.gray_code:
            val = self.binaryToGray(val)
        freq = self.value_frequencies[val]
        symbol = self.generateSignalRaw(freq, 0, self.symbol_width)
        if self.smoothing:
            symbol = self.smoothSignal(symbol)
        return symbol

    def parseSymbol(self, symbol: np.ndarray) -> int:
        assert symbol.size == self.symbol_width
        band_width = 100 // self.baud # 100Hz = 'band width' for symbol frequencies
        window = scipy.signal.hamming(self.symbol_width)
        mag = abs(np.fft.rfft(symbol*window))
        frequencies = np.floor(np.array(self.value_frequencies) / self.baud)
        frequencies = np.concatenate(([self.termination_freq / self.baud], frequencies))
        amplitudes = [np.max(mag[freq - (band_width//2):freq + 1 + (band_width//2)])
            for freq in frequencies.astype(np.int)]
        val = np.argmax(amplitudes) - 1
        if self.gray_code:
            val = self.grayToBinary(val)
        return val

class PSK(Modulation):
    def __init__(self, bits_per_symbol: int, baud: int, smoothing: bool = True,
                 gray_code: bool = True,
                 sample_rate: int = DEFAULT_SAMPLE_RATE) -> None:
        super(PSK, self).__init__(bits_per_symbol, baud, smoothing, gray_code,
                                  sample_rate)
        # Declare variables
        self.sync_symbol = None
        self.termination_symbol = None
        self._carrier_freq = 1000
        self._sync_freq = 0
        self._termination_freq = 0
        # Initialise variables
        self.sync_freq = 1500
        self.termination_freq = 1800

    @property
    def sync_freq(self):
        return self._sync_freq

    @sync_freq.setter
    def sync_freq(self, freq):
        self._sync_freq = freq
        self.sync_symbol = self.generateSignalRaw(freq, 0, self.symbol_width)

    @property
    def termination_freq(self):
        return self._termination_freq

    @termination_freq.setter
    def termination_freq(self, freq):
        self._termination_freq = freq
        self.termination_symbol = self.generateSignalRaw(freq, 0, self.symbol_width)

    def _phaseToVal(self, phase: float) -> int:
        assert phase >= 0 and phase <= 2 * np.pi
        angle_increment = 2 * np.pi / (self.symbols_max_val + 1)
        vals = np.arange(0, (self.symbols_max_val+1.1) * angle_increment, angle_increment)
        val = np.argmin(np.abs(vals - phase)) % (self.symbols_max_val + 1)
        return val

    def generateSymbol(self, val: int) -> np.ndarray:
        assert val >= 0 and val <= self.symbols_max_val, "%d is out of range" % val
        if self.gray_code:
            val = self.binaryToGray(val)
        phase = 2*np.pi * (val / (self.symbols_max_val + 1))
        symbol = self.generateSignalRaw(self._carrier_freq, phase,
                                        self.symbol_width)
        if self.smoothing:
            symbol = self.smoothSignal(symbol)
        return symbol

    def parseSymbol(self, symbol: np.ndarray) -> int:
        zero_symbol = self.generateSymbol(0)
        # Correlate with zero symbol
        correlation = scipy.signal.correlate(zero_symbol, symbol, mode='full')
        sync_index = np.argmax(correlation)
        sync_correlation = correlation[sync_index]
        perfect_correlation = np.sum(zero_symbol ** 2)
        if sync_correlation <= 0.1 * perfect_correlation:
            return -1
        samples_per_oscillation = self.sample_rate / self._carrier_freq
        phase = 2 * np.pi * ((sync_index % samples_per_oscillation)\
            / samples_per_oscillation)
        val = self._phaseToVal(phase)
        if self.gray_code:
            val = self.grayToBinary(val)
        return val

class OOK(Modulation):
    def __init__(self, baud: int, smoothing: bool = True,
            sample_rate: int = DEFAULT_SAMPLE_RATE) -> None:
        bits_per_symbol = 1
        super(OOK, self).__init__(bits_per_symbol, baud, smoothing, True, sample_rate)
        self.base_freq = 1000 #Hz
        self.sync_freq = int(self.base_freq * 1.5)
        self.termination_freq = int(self.base_freq * 2.5)
        self.sync_symbol = self.generateSignalRaw(self.sync_freq, 0, self.symbol_width)
        self.termination_symbol = self.generateSignalRaw(self.termination_freq, 0, self.symbol_width)
        self.on_symbol = self.generateSignalRaw(self.base_freq, np.pi, self.symbol_width)
        self.off_symbol = self.generateSignalRaw(1.0, np.pi, self.symbol_width)

    def generateSymbol(self, val: int) -> np.ndarray:
        assert val >= 0 and val <= self.symbols_max_val, "Val is out of range"
        symbol = None
        if val == 1:
            symbol = self.on_symbol
        else:
            symbol = self.off_symbol
        if self.smoothing:
            symbol = self.smoothSignal(symbol)
        return symbol

    def parseSymbol(self, symbol: np.ndarray) -> int:
        # Correlate with on symbol
        correlation = scipy.signal.correlate(self.on_symbol, symbol,
                                             mode='full')
        max_correlation_index = np.argmax(correlation)
        max_correlation = correlation[max_correlation_index]
        perfect_correlation = np.sum(self.on_symbol ** 2)
        if max_correlation > 0.1 * perfect_correlation:
            return 1
        # Correlate with termination symbol
        correlation = scipy.signal.correlate(self.termination_symbol, symbol,
                                             mode='full')
        max_correlation_index = np.argmax(correlation)
        max_correlation = correlation[max_correlation_index]
        perfect_correlation = np.sum(self.termination_symbol ** 2)
        if max_correlation > 0.1 * perfect_correlation:
            return -1
        # If neither symbols have strong correlations, return 0
        return 0

class _Frame:
    def __init__(self, byte_string: bytes, network: NetworkDescription) -> None:
        # Check byte_string contains data, and has length <= max length
        assert byte_string
        self.frame_byte_capacity = network.frame_byte_capacity
        assert len(byte_string) <= self.frame_byte_capacity
        self._byte_string = byte_string
        self.network = network
        self.modulator = network.modulator
        self.encoding = network.encoding
        self._signal = np.zeros(
            network.frame_length_total * self.modulator.symbol_width,
            dtype=np.float32
        )
        self._symbols_header = np.zeros(
            (network.frame_length_header, self.modulator.symbol_width),
            dtype=np.float32
        )
        self._symbols_payload = np.zeros(
            (network.frame_length_payload, self.modulator.symbol_width),
            dtype=np.float32
        )
        self._payload_length = None

    @property
    def byte_string(self):
        return self._byte_string

    @property
    def payload_length(self):
        if self._payload_length is None:
            self._generateDataSymbols()
        return self._payload_length

    def _generateDataSymbols(self) -> None:
        encoded_bytes = self.encoding.encode(self._byte_string)
        assert len(encoded_bytes) <= (self.network.frame_length_payload \
                                      // self.modulator.symbols_per_byte)
        vals = split_byte_string(encoded_bytes, self.modulator.bits_per_symbol)
        symbols = [self.modulator.generateSymbol(val) for val in vals]
        self._symbols_payload[:vals.size] = np.array(symbols)
        if vals.size < self.network.frame_length_payload:
            self._symbols_payload[vals.size] = self.modulator.termination_symbol
        self._payload_length = vals.size

    def _generateHeaderSymbols(self) -> None:
        self._symbols_header[0, :] = self.modulator.sync_symbol

    def getDataSymbols(self) -> np.ndarray:
        if not self._symbols_payload.any():
            self._generateDataSymbols()
        return self._symbols_payload

    def getAudio(self) -> np.ndarray:
        if not self._symbols_payload.any():
            self._generateDataSymbols()
        if not self._symbols_header.any():
            self._generateHeaderSymbols()
        if not self._signal.any():
            symbols = np.concatenate((self._symbols_header, self._symbols_payload))
            self._signal = np.concatenate(symbols)
        return self._signal

    def __repr__(self) -> str:
        vals = split_byte_string(self._byte_string, self.modulator.bits_per_symbol)
        message = 'S,' + ','.join([str(val) for val in vals])
        return message

class _FrameParser: # pylint: disable=too-few-public-methods
    def __init__(self, signal: np.ndarray, network: NetworkDescription) -> None:
        self._signal = signal
        self._symbols: List[np.ndarray] = []
        self._header_symbols: List[np.ndarray] = []
        self._payload_symbols: List[np.ndarray] = []
        self._payload_vals: np.ndarray = np.array([])
        self._decoding_success: bool = None
        self._payload_bytes: bytes = b''
        self._message_bytes: bytes = b''
        self.network = network
        self.amplify = network.amplify
        self.modulator = network.modulator
        self.encoding = network.encoding
        frame_length_samples_total = self.modulator.symbol_width \
                                     * network.frame_length_total
        assert signal.shape[0] == frame_length_samples_total
        if self.amplify:
            max_amplitude = np.max(self._signal)
            self._signal = self._signal / max_amplitude

    def _composeByteString(self, vals: np.ndarray) -> bytes:
        assert vals.size % self.modulator.symbols_per_byte == 0
        return compose_byte_string(vals, self.modulator.bits_per_symbol)

    def _parseSignal(self) -> None:
        indices = np.arange(0, self._signal.size, self.modulator.symbol_width)[1:]
        self._symbols = np.split(self._signal, indices)
        self._header_symbols = self._symbols[:self.network.frame_length_header]
        self._payload_symbols = self._symbols[self.network.frame_length_header\
                                           :self.network.frame_length_total]

    def _generateByteString(self) -> None:
        if not self._payload_symbols:
            self._parseSignal()
        vals = np.array([self.modulator.parseSymbol(symbol)
            for symbol in self._payload_symbols])
        terminators = vals < 0
        if terminators.any():
            self._payload_vals = vals[:np.argmax(terminators)]
        else:
            self._payload_vals = vals[:self.network.frame_length_payload]
        if self._payload_vals.size % self.modulator.symbols_per_byte == 0:
            self._payload_bytes = self._composeByteString(self._payload_vals)
        else:
            self._payload_bytes = b''
        success, self._message_bytes = self.encoding.decode(self._payload_bytes)
        self._decoding_success = success

    def getDataSymbols(self) -> List[np.ndarray]:
        if not self._payload_symbols:
            self._parseSignal()
        return self._payload_symbols

    def getByteString(self) -> bytes:
        if self._payload_vals.size == 0:
            self._generateByteString()
        return self._message_bytes

    @property
    def byte_string(self):
        return self.getByteString()

    def getDecodingSuccessful(self) -> bool:
        if self._decoding_success is None:
            self._generateByteString()
        return self._decoding_success

    def __repr__(self) -> str:
        if self._payload_vals.size == 0:
            self._generateByteString()
        message = 'S,' + ','.join([str(val) for val in self._payload_vals])
        return message

class Transmitter:
    def __init__(self, network: NetworkDescription) -> None:
        self.network = network
        self.modulator = network.modulator
        self.frame_byte_capacity = network.frame_byte_capacity
        self.chunk_size = network.sample_rate // 10
        noise = np.random.uniform(size=(self.chunk_size,)) / 1000
        self._noise = noise.astype(np.float32)
        # Control Objects
        self._py_audio: pyaudio.PyAudio = None
        self._stream: pyaudio.Stream = None
        self._flag = pyaudio.paContinue
        self._stream_lock = Lock()
        self._data_lock = Lock()
        self._flag_lock = Lock()
        # Data Objects
        self._audio: np.ndarray = np.zeros((0,), dtype=np.float32)
        self._frame_current: int = 0

    @property
    def active(self):
        if self._stream:
            return self._stream.is_active()
        return False

    def _generateFrames(self, byte_string: bytes) -> List[_Frame]:
        byte_count = len(byte_string)
        frame_count = (byte_count // self.frame_byte_capacity)
        if byte_count % self.frame_byte_capacity != 0:
            frame_count += 1
        frames = []
        for i in range(frame_count):
            start_index = i*self.frame_byte_capacity
            end_index = start_index + self.frame_byte_capacity
            frame_string = byte_string[start_index:end_index]
            frame = _Frame(frame_string, self.network)
            frames.append(frame)
        return frames

    def generateAudio(self, byte_string: bytes) -> np.ndarray:
        frames = self._generateFrames(byte_string)
        audio = np.concatenate([frame.getAudio() for frame in frames])
        return audio

    def transmit(self, byte_string: bytes) -> float:
        with self._stream_lock:
            if not self.active:
                raise Exception("Transmitter not initialised! "+\
                                "Call Transmitter.start()")
            audio = self.generateAudio(byte_string)
            with self._data_lock:
                self._audio = np.concatenate((self._audio, audio))
        return audio.size / self.network.sample_rate

    def _streamCallback(self, in_data, frame_count, time_info, status): # pylint: disable=unused-argument
        data = np.array(self._noise, dtype=np.float32)
        with self._flag_lock:
            if self._flag != pyaudio.paContinue:
                return (np.array([], dtype=np.float32), self._flag)
        with self._data_lock:
            audio = self._audio[self._frame_current:self._frame_current + frame_count]
            data[:audio.size] = audio
            self._frame_current += frame_count
            if audio.size < frame_count:
                self._audio = np.zeros((0,), dtype=np.float32)
                self._frame_current = 0
        return (data, pyaudio.paContinue)

    def start(self) -> None:
        with self._stream_lock:
            if self.active:
                print("Transmitter already initialised!")
                return
            self._frame_current = 0
            self._flag = pyaudio.paContinue
            self._py_audio = pyaudio.PyAudio()
            self._stream = self._py_audio.open(format=pyaudio.paFloat32,
                                        channels=1,
                                        rate=self.network.sample_rate,
                                        output=True,
                                        frames_per_buffer=self.chunk_size,
                                        stream_callback=self._streamCallback,
                                        start=True)

    def stop(self) -> None:
        with self._stream_lock:
            with self._flag_lock:
                self._flag = pyaudio.paComplete
            # Wait for stream to die
            while self._stream.is_active():
                time.sleep(0.1)
            # Close stream and pyaudio
            self._stream.stop_stream()
            self._stream.close()
            self._py_audio.terminate()
            self._py_audio = None

    def __enter__(self) -> 'Transmitter':
        # For with statements
        self.start()
        return self

    def __exit__(self, exception_type, exception_value, traceback) -> None:
        # For with statements
        self.stop()

class _SignalParser:
    def __init__(self, signal: np.ndarray, network: NetworkDescription) -> None:
        self.network = network
        self.modulator = network.modulator
        self.frame_width: int = network.frame_length_total * self.modulator.symbol_width
        self.signal = signal
        self._frames: Dict[int, _FrameParser] = {}

    def findFrames(self) -> List[int]:
        # Correlate with frame sync
        padded_signal = pad_to_nextpow2(self.signal)
        correlation = scipy.signal.correlate(
            padded_signal, self.modulator.sync_symbol, mode='valid')
        correlation = correlation[:self.signal.shape[0]]
        standard_deviation = np.std(correlation)
        mean = np.mean(correlation)
        sync_index = np.argmax(correlation)
        sync_correlation = correlation[sync_index]
        perfect_correlation = np.sum(self.modulator.sync_symbol ** 2)
        if sync_correlation <= 0.1 * perfect_correlation:
            return []
        frame_phase = sync_index % self.frame_width
        frame_indices = []
        for i in range(frame_phase, self.signal.shape[0], self.frame_width):
            if correlation[i] >= 0.1 * perfect_correlation:
                frame_indices.append(i)
        return frame_indices

    def getFrames(self) -> Dict[int, _FrameParser]:
        if self._frames:
            return self._frames
        frame_indices = self.findFrames()
        for i in frame_indices:
            if i + self.frame_width > self.signal.shape[0]:
                break
            frame_signal = self.signal[i:i+self.frame_width]
            self._frames[i] = _FrameParser(frame_signal, self.network)
        return self._frames

    def getBytes(self) -> bytes:
        frames = self.getFrames()
        strings = [frame.getByteString() for frame in frames.values()]
        if strings:
            byte_string = reduce(lambda x, y: x+y, strings)
        else:
            byte_string = b''
        return byte_string

    def __repr__(self) -> str:
        frames = self.getFrames()
        message = ''.join([
            "%f: %s\n" % (i/self.network.sample_rate, frames[i]) for i in frames])
        return message

class _RealtimeReceiver:
    def __init__(self, callback, network: NetworkDescription):
        self.callback = callback
        self.network = network
        self.modulator = network.modulator
        self.chunk_size = self.network.sample_rate
        self.buffer_length = self.chunk_size \
            * ((self.modulator.symbol_width * network.frame_length_total\
                // self.chunk_size) + 1)
        # Control Objects
        self.py_audio: pyaudio.PyAudio
        self.stream: pyaudio.Stream
        self.flag_lock = Lock()
        self.flag: int = pyaudio.paContinue
        # Data Objects
        self.next_frame: int = self.buffer_length
        self.buffer: np.ndarray = np.ones((self.buffer_length,))

    def _processBuffer(self):
        signal_parser = _SignalParser(self.buffer, self.network)
        frame_indices = signal_parser.findFrames()
        if not frame_indices:
            self.next_frame = self.buffer_length - self.chunk_size + 1
            return
        if frame_indices[0] < self.chunk_size:
            byte_string = signal_parser.getBytes()
            self.callback(byte_string)
            # Set next frame
            frames = signal_parser.getFrames()
            if len(frame_indices) > len(frames):
                self.next_frame = frame_indices[len(frames)]
            else:
                self.next_frame = self.buffer_length
        else:
            self.next_frame = frame_indices[0]

    def _streamCallback(self, in_data, frame_count, time_info, status):
        with self.flag_lock:
            flag = self.flag
        audio_segment = np.fromstring(in_data, dtype=np.float32)
        assert audio_segment.shape[0] == self.chunk_size
        self.buffer = np.roll(self.buffer, -self.chunk_size)
        self.buffer[-self.chunk_size:] = audio_segment
        self.next_frame -= self.chunk_size
        if self.next_frame < self.chunk_size:
            self._processBuffer()
        return (None, flag)

    def start(self) -> None:
        with self.flag_lock:
            self.flag = pyaudio.paContinue
        self.next_frame: int = self.buffer_length
        self.py_audio = pyaudio.PyAudio()
        self.stream = self.py_audio.open(format=pyaudio.paFloat32,
                                         channels=1,
                                         rate=self.network.sample_rate,
                                         frames_per_buffer=self.chunk_size,
                                         input=True,
                                         stream_callback=self._streamCallback)
        self.stream.start_stream()

    def stop(self) -> None:
        with self.flag_lock:
            self.flag = pyaudio.paAbort
        # Wait for stream to die
        while self.stream.is_active():
            time.sleep(0.1)
        # Close stream and pyaudio
        self.stream.stop_stream()
        self.stream.close()
        self.py_audio.terminate()

class Receiver:
    buffer_size = 2000
    def __init__(self, network: NetworkDescription) -> None:
        self._data_lock = Lock()
        self._data_received = np.zeros((self.buffer_size,), dtype=np.uint8)
        self._byte_count = 0
        self._receiver_lock = Lock()
        self._realtime_receiver = _RealtimeReceiver(self._saveToBuffer, network)
        self.network = network
        self.modulator = network.modulator

    def retrieveByteStringFromSignal(self, signal: np.ndarray) -> bytes:
        signal_parser = _SignalParser(signal, self.network)
        return signal_parser.getBytes()

    def _convertFormatToFloat(self, signal: np.ndarray) -> np.ndarray:
        dtype = signal.dtype
        if dtype == np.float32:
            print("Format already float32")
            return signal
        audio = signal.astype(np.float32)
        if dtype == np.uint8:
            audio = ((2 * audio) / (1 << 8)) + 1
        if dtype == np.int16:
            audio = audio / (1 << 15)
        if dtype == np.int32:
            audio = audio / (1 << 31)
        return audio

    def _padAudio(self, audio: np.ndarray) -> np.ndarray:
        n = audio.shape[0]
        y = np.floor(np.log2(n))
        nextpow2 = int(np.power(2, y+1))
        padded_audio = np.pad(audio, (0, nextpow2-n), mode='constant')
        return padded_audio

    def _reformatSignal(self, data: np.ndarray, rate: int) -> np.ndarray:
        audio = self._convertFormatToFloat(data)
        if rate != self.network.sample_rate:
            padded_audio = self._padAudio(audio)
            num_samples = np.floor(self.network.sample_rate * padded_audio.size / rate).astype(int)
            audio = scipy.signal.resample(padded_audio, num_samples)
        return audio

    def retrieveByteStringFromWav(self, filename: str) -> np.ndarray:
        rate, data = scipy.io.wavfile.read(filename)
        audio = self._reformatSignal(data, rate)
        return self.retrieveByteStringFromSignal(audio)

    def getBuffer(self) -> bytes:
        with self._data_lock:
            data_received = bytes(self._data_received[:self._byte_count])
        return data_received

    def _saveToBuffer(self, byte_string: bytes) -> None:
        data = np.array(list(byte_string), dtype=np.uint8)
        with self._data_lock:
            if self._byte_count < self.buffer_size:
                self._data_received[self._byte_count:self._byte_count+data.size] = data
            else:
                self._data_received = np.roll(self._data_received, data.size)
                self._data_received[-data.size:] = data
            self._byte_count += data.size

    def startRealtime(self, callback=None):
        if not callback:
            callback = self._saveToBuffer
        with self._receiver_lock:
            self._realtime_receiver = _RealtimeReceiver(callback, self.network)
            self._realtime_receiver.start()

    def stopRealtime(self):
        with self._receiver_lock:
            self._realtime_receiver.stop()

